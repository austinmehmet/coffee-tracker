const coffees = [
    {
        name: 'Monkey Nuts',
        roaster: 'Gorilla Roasters - Seattle',
        roast: 'Bold',
        recipes: [
            {
                coffeeIn: 18,
                coffeeOut: 40,
                time: 120,
                flavor: 'Sweet',
                notes: 'Fruit, Chocolate, whiskey',
                texture: 'thin',
                rating: '5'
            }
        ]
    }
]

