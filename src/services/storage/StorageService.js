import { AsyncStorage } from 'react-native'

const store = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value))
  } catch (e) {
    console.error('Cannot store data into local storage', e.message)
  }
}

const read = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key)
    if (key === 'coffees' && !value) {
      return []
    }
    return JSON.parse(value)
  } catch (e) {
    console.error('Cannot read data from local storage', e.message)
    return Promise.reject()
  }
}

const remove = async (key) => {
  try {
    await AsyncStorage.removeItem(key)
  } catch (e) {
    console.error('Cannot remove data from local storage', e.message)
  }
}

const clear = async () => {
  await AsyncStorage.clear()
}

export default {
  store, read, remove, clear,
}
