const validate = (obj) => {
  const validator = { isValid: true, msgs: [] }

  if (!obj.name) {
    validator.isValid = false
    validator.msgs.push('Missing "Coffee Name" Field')
  }

  if (!obj.roaster) {
    validator.isValid = false
    validator.msgs.push('Missing "Coffee Roaster" Field')
  }

  if (!obj.roast) {
    validator.isValid = false
    validator.msgs.push('Missing "Roast" Field')
  }

  return validator
}

export default { validate }
