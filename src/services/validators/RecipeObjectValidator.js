const validate = (obj) => {
  const validator = { isValid: true, msgs: [] }

  if (!obj.coffeeIn) {
    validator.isValid = false
    validator.msgs.push('Missing "Coffee In" Field')
  }

  if (!obj.coffeeOut) {
    validator.isValid = false
    validator.msgs.push('Missing "Coffee Out" Field')
  }

  if (!obj.time) {
    validator.isValid = false
    validator.msgs.push('Missing "Time" Field')
  }

  return validator
}

export default { validate }
