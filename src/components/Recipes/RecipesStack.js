import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import RecipesScreen from './RecipesScreen'
import CoffeesScreen from './CoffeesScreen'
import AddRecipe from './AddRecipe'
import AddCoffee from './AddCoffee'

const Stack = createStackNavigator()

export default function RecipesStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Coffees" component={CoffeesScreen} />
      <Stack.Screen name="Recipes" component={RecipesScreen} />
      <Stack.Screen name="Add a Coffee" component={AddCoffee} />
      <Stack.Screen name="Add a Recipe" component={AddRecipe} />
    </Stack.Navigator>
  )
}
