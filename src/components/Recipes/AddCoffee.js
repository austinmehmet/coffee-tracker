import * as React from 'react'
import { Text, View, Button } from 'react-native'
import { Input } from 'react-native-elements'

import StorageService from '../../services/storage/StorageService'
import CoffeeObjectValidator from '../../services/validators/CoffeeObjectValidator'

export default function AddCoffee({ route, navigation }) {
  const { update } = route.params
  const [errorMsgs, errorMsgsOnChange] = React.useState([])
  const [name, nameOnChange] = React.useState()
  const [roaster, roasterOnChange] = React.useState()
  const [roast, roastOnChange] = React.useState()

  const saveCoffee = async () => {
    const newCoffee = {
      name,
      roaster,
      roast,
      recipies: [],
    }

    const validation = CoffeeObjectValidator.validate(newCoffee)
    if (!validation.isValid) {
      errorMsgsOnChange(validation.msgs)
      return
    }

    const coffees = await StorageService.read('coffees')
    coffees.push(newCoffee)
    await StorageService.store('coffees', coffees)
    await update()
    navigation.navigate('Coffees')
  }

  return (
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <Input
        placeholder="Coffee Name"
        value={name}
        onChangeText={(text) => nameOnChange(text)}
      />
      <Input
        placeholder="Coffee Roaster"
        value={roaster}
        onChangeText={(text) => roasterOnChange(text)}
      />
      <Input
        placeholder="Coffee Roast (Bold, Medium, Light)"
        value={roast}
        onChangeText={(text) => roastOnChange(text)}
      />
      {errorMsgs.length > 0 && errorMsgs.map((msg) => <Text style={{ color: 'red' }}>{msg}</Text>)}
      <Text>{'\n'}</Text>
      <Button
        style={{ width: '75px' }}
        title="                    Add                    "
        onPress={saveCoffee}
      />
    </View>
  )
}
