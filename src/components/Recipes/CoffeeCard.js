import * as React from 'react'
import {
  View, Text, Alert, TouchableOpacity,
} from 'react-native'
import {
  Card, Button, Icon, Tile, Divider
} from 'react-native-elements'

export default function CoffeeCard({ coffee, navigation, removeCoffee }) {
  const deleteCoffee = () => {
    Alert.alert(
      'Are you sure?',
      'This will delete this coffee and all recipes associated with it',
      [
        { text: 'OK', onPress: () => removeCoffee(coffee) },
        { text: 'Cancel', onPress: () => { } },
      ],
    )
  }

  return (
    <Card
      title={<>
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <Text
            style={{ fontWeight: 'bold', fontSize: 30, width: 150 }}>
            {coffee.name}</Text>
          <TouchableOpacity
            style={{ marginLeft: 'auto' }}
            onPress={deleteCoffee}>
            <Icon
              name="ios-trash"
              type="ionicon"
              size={40}
              color="red"
            />
          </TouchableOpacity>
        </View>
        <Divider style={{ backgroundColor: 'gray' }} />
      </>}
    >
      <Text style={{marginTop: 10}}>
        <Text style={{ fontWeight: 'bold' }}>Roasted by:</Text>
        {' '}
        {coffee.roaster}
      </Text>
      <Text>
        <Text style={{ fontWeight: 'bold' }}>Roast level: </Text>
        {' '}
        {coffee.roast}
      </Text>
      <View style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'center' }}>
        <Button
          buttonStyle={{backgroundColor: "#D7C3A1"}}
          onPress={() => navigation.navigate('Recipes', { coffee })}
          icon={
            <Icon
              name="ios-journal"
              type="ionicon"
              size={15}
              color="white"
            />
          }
          title="   Recipes"
        />
      </View>
    </Card>
  )
}
