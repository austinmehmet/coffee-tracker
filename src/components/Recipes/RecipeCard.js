import * as React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import {
  Card, Icon, Divider, Rating
} from 'react-native-elements'

export default function RecipeCard({ recipe, id, deleteRecipe }) {
  return (
    <Card
      title={<>
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <Text
            style={{ fontWeight: 'bold', fontSize: 30, width: 150 }}>
            {`#${id}`}
          </Text>
          <TouchableOpacity
            style={{ marginLeft: 'auto' }}
            onPress={() => deleteRecipe(--id)}>
            <Icon
              name="ios-trash"
              type="ionicon"
              size={40}
              color="red"
            />
          </TouchableOpacity>
        </View>
        <Divider style={{ backgroundColor: 'gray' }} />
      </>}
      key={id}>
      <Text style={{ marginBottom: 10, marginTop: 10 }}>
        <Text style={{ fontWeight: 'bold' }}>{'Coffee In: '}</Text>
        {recipe.coffeeIn}
        (g)
      </Text>
      <Text style={{ marginBottom: 10 }}>
        <Text style={{ fontWeight: 'bold' }}>{'Yield: '}</Text>
        {recipe.coffeeOut}
        {'(g)'}
      </Text>
      <Text style={{ marginBottom: 10 }}>
        <Text style={{ fontWeight: 'bold' }}>{'Time: '}</Text>
        {recipe.time}
        (s)
      </Text>
      <Text style={{ marginBottom: 10 }}>
        <Text style={{ fontWeight: 'bold' }}>{'Flavor: '}</Text>
        {recipe.flavor}
      </Text>
      <Text style={{ marginBottom: 10 }}>
        <Text style={{ fontWeight: 'bold' }}>{'Tasting Notes: '}</Text>
        {recipe.notes}
      </Text>
      <Text style={{ marginBottom: 10 }}>
        <Text style={{ fontWeight: 'bold' }}>{'Texture: '}</Text>
        {recipe.texture}
      </Text>
      <Rating
        readonly
        startingValue={recipe.rating}
        imageSize={30}>
      </Rating>
    </Card>
  )
}
