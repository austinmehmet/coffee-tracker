import * as React from 'react'
import {
  Text, View, Button, SafeAreaView, ScrollView
} from 'react-native'
import {
  AirbnbRating, Slider, Input,
} from 'react-native-elements'

import StorageService from '../../services/storage/StorageService'
import RecipeObjectValidator from '../../services/validators/RecipeObjectValidator'

export default function AddRecipe({ route, navigation }) {
  const { coffee } = route.params
  const { update } = route.params
  const [errorMsgs, errorMsgsOnChange] = React.useState([])
  const [coffeeIn, coffeeInOnChange] = React.useState()
  const [coffeeOut, coffeeOutOnChange] = React.useState()
  const [time, timeOnChange] = React.useState()
  const [flavor, flavorOnChange] = React.useState(2)
  const [notes, notesOnChange] = React.useState()
  const [texture, textureOnChange] = React.useState(2)
  const [rating, ratingOnChange] = React.useState(3)

  const saveRecipe = async () => {
    const flavorToAdd = flavor === 1 ? 'Acidic' : flavor === 2 ? 'Sweet' : 'Bitter'
    const textureToAdd = texture === 1 ? 'Thin' : texture === 2 ? 'Neutral' : 'Thick'
    const recipeToSave = {
      coffeeIn,
      coffeeOut,
      time,
      flavor: flavorToAdd,
      notes,
      texture: textureToAdd,
      rating,
    }

    const validation = RecipeObjectValidator.validate(recipeToSave)
    if (!validation.isValid) {
      errorMsgsOnChange(validation.msgs)
      return
    }

    const coffees = await StorageService.read('coffees')
    const coffeeToEdit = coffees.find((el) => el.name === coffee.name)
    if (!coffeeToEdit.recipes) {
      coffeeToEdit.recipes = []
    }
    coffeeToEdit.recipes.push(recipeToSave)
    await StorageService.store('coffees', coffees)
    await update()
    navigation.navigate('Recipes')
  }

  return (
    <SafeAreaView>
      <ScrollView contentContainerStyle={{ justifyContent: 'center', alignItems: 'center' }}>
        <Input
          keyboardType="number-pad"
          placeholder="Coffee in (grams)"
          value={coffeeIn}
          onChangeText={(text) => coffeeInOnChange(text)}
        />
        <Input
          keyboardType="number-pad"
          placeholder="Coffee out (grams)"
          value={coffeeOut}
          onChangeText={(text) => coffeeOutOnChange(text)}
        />
        <Input
          keyboardType="number-pad"
          placeholder="Total Time Taken (seconds)"
          value={time}
          onChangeText={(text) => timeOnChange(text)}
        />
        <Text>Flavor</Text>
        <View style={{ width: '75%' }}>
          <Slider
            value={flavor}
            onValueChange={(value) => flavorOnChange(value)}
            minimumValue={1}
            maximumValue={3}
            step={1}
            animateTransitions={true}
          />
          <Text>{flavor === 1 ? 'Acidic' : flavor === 2 ? 'Sweet' : 'Bitter'}</Text>
        </View>
        <Text>Texture</Text>
        <View style={{ width: '75%' }}>
          <Slider
            value={texture}
            onValueChange={(value) => textureOnChange(value)}
            minimumValue={1}
            maximumValue={3}
            step={1}
            animateTransitions={true}
          />
          <Text>{texture === 1 ? 'Thin' : texture === 2 ? 'Neutral' : 'Thick'}</Text>
        </View>
        <Input
          placeholder="Notes Tasted"
          value={notes}
          onChangeText={(text) => notesOnChange(text)}
        />
        <Text>Overall Rating</Text>
        <AirbnbRating
          count={5}
          showRating={false}
          defaultRating={3}
          size={35}
          onFinishRating={(finishedRating) => ratingOnChange(finishedRating)}
        />
        {errorMsgs.length > 0 && errorMsgs.map((msg) => <Text key={msg} style={{ color: 'red' }}>{msg}</Text>)}
        <Button
          style={{ width: '75px' }}
          title="                    Add                    "
          onPress={saveRecipe}
        />
      </ScrollView>
    </SafeAreaView>
  )
}
