import * as React from 'react'
import {
  Text, SafeAreaView, ScrollView, Alert, TouchableOpacity
} from 'react-native'
import {
  Card, Button, Icon,
} from 'react-native-elements'

import RecipeCard from './RecipeCard'
import StorageService from '../../services/storage/StorageService'

export default function RecipesScreen({ route, navigation }) {
  const { coffee } = route.params
  const startingRecipes = coffee.recipes ? coffee.recipes : []

  const [recipes, recipesOnChange] = React.useState(startingRecipes)

  React.useEffect(() => {
    const readStorage = async () => {
      const coffees = await StorageService.read('coffees')
      let ourRecipes = coffees.find((el) => el.name === coffee.name).recipes
      if (!ourRecipes) {
        ourRecipes = []
      }
      recipesOnChange(ourRecipes)
    }
    readStorage()
  }, [recipes])

  const update = async () => {
    const coffees = await StorageService.read('coffees')
    let updatedRecipies = coffees.find((el) => el.name === coffee.name).recipes
    if (!updatedRecipies) {
      updatedRecipies = []
    }
    recipesOnChange(updatedRecipies)
  }

  const removeRecipe = async (index) => {
    const coffees = await StorageService.read('coffees')
    const ourCoffee = coffees.find((el) => el.name === coffee.name)
    ourCoffee.recipes.splice(index, 1)
    await StorageService.store('coffees', coffees)
    recipesOnChange(ourCoffee.recipes)
  }

  const deleteRecipe = async (index) => {
    Alert.alert(
      'Are you sure?',
      'This will remove this recipe permanently',
      [
        { text: 'OK', onPress: async () => removeRecipe(index) },
        { text: 'Cancel', onPress: () => { } },
      ],
    )
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView>
        {
          recipes.map((recipe, i) =>
            <RecipeCard recipe={recipe} id={++i} key={i} deleteRecipe={deleteRecipe} />
          )
        }
        <Text>{'\n'}</Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('Add a Recipe', { coffee, update })}>
          <Icon
            name="ios-add-circle"
            type="ionicon"
            size={50}
            color="#147EFB">
          </Icon>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  )
}
