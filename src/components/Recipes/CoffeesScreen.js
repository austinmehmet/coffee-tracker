import * as React from 'react'
import {
  Text, Button, SafeAreaView, ScrollView, TouchableOpacity,
} from 'react-native'

import CoffeeCard from './CoffeeCard'
import StorageService from '../../services/storage/StorageService'
import { Icon } from 'react-native-elements'

export default function CoffeesScreen({ navigation }) {
  let [coffees, coffeesOnChange] = React.useState([])

  React.useEffect(() => {
    const readStorage = async () => {
      const storedCoffees = await StorageService.read('coffees')
      coffeesOnChange(storedCoffees)
    }
    readStorage()
  }, [coffees])

  const update = async () => {
    coffees = await StorageService.read('coffees')
  }

  const removeCoffee = async (coffee) => {
    const allCoffees = await StorageService.read('coffees')
    const coffeeRemoved = allCoffees.filter((el) => el.name !== coffee.name)
    coffeesOnChange(coffeeRemoved)
    await StorageService.store('coffees', coffeeRemoved)
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView>
        {coffees.map((el, i) => <CoffeeCard coffee={el} navigation={navigation} key={el.name + i} update={update} removeCoffee={removeCoffee} />)}
        <Text>{'\n'}</Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('Add a Coffee', { update })}>
          <Icon
            name="ios-add-circle"
            type="ionicon"
            size={50}
            color="#147EFB">
          </Icon>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  )
}
