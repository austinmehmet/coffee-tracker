import * as React from 'react'
import {
  Text, View, TouchableOpacity, SafeAreaView, ScrollView,
} from 'react-native'
import { Card } from 'react-native-elements'
import { AdMobBanner } from 'expo-ads-admob'

import CoffeeFacts from '../../constants/CoffeeFacts'

export default function HomeScreen() {
  const [factIndex, factIndexOnChange] = React.useState(Math.floor(Math.random() * CoffeeFacts.facts.length))
  const fact = CoffeeFacts.facts[factIndex]


  return (
    <SafeAreaView>
      <ScrollView>
        <AdMobBanner
          bannerSize="fullBanner"
          adUnitID="ca-app-pub-0611504067044208/4502073670"
          servePersonalizedAds={true}
          onDidFailToReceiveAdWithError={() => console.log('Failed to display ad')}
        />
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Card
            title="Welcome"
            image={require('../../../assets/espresso-drip.jpg')}
          >
            <Text style={{ marginBottom: 10 }}>
              Track all of your espresso recipes with ease.
              This app will help you keep a log of all your recipes when dialing in your espresso.
              Refer back to this log to ensure you make the perfect espresso everytime!
            </Text>
          </Card>
          <Text>{'\n\n'}</Text>
          <TouchableOpacity onPress={() => factIndexOnChange(Math.floor(Math.random() * CoffeeFacts.facts.length))}>
            <Card
              title="Random Coffee Fact"
            >
              <Text style={{ marginBottom: 10 }}>{fact}</Text>
            </Card>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
