import * as React from 'react'
import {
  View, Button, Alert,
} from 'react-native'

import StorageService from '../../services/storage/StorageService'

export default function SettingsScreen() {
  const deleteCoffees = async () => {
    await StorageService.clear()
  }

  const clearData = () => {
    Alert.alert(
      'Are you sure?',
      'This will delete all local data for this application including coffees and recipes stored',
      [
        { text: 'OK', onPress: () => deleteCoffees() },
        { text: 'Cancel', onPress: () => {} },
      ],
    )
  }

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Button
        buttonStyle={{
          borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0,
        }}
        title="Delete all coffees"
        onPress={clearData}
      />
    </View>
  )
}
